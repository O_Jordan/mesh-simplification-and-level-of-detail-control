#pragma once
#include "obj.h"
#include <iostream>
#include <string>
#include "MathLib.h"
#include "Vector.h"
class objNewReader
{
private:

public:
	objNewReader();
	~objNewReader();
	MathLib myMaths;
	//read file, pass back resulting obj from file
	obj readObjFile(char * inFile);

	void calculateVertexImportance(obj * object);

	void collapseObject(obj * object, float percentage, float cellSize);
};
