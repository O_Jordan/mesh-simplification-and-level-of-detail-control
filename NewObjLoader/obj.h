#pragma once

class obj
{
private:
public:
	obj(int inVSize, int inVTSize, int inVNSize, int inFSize);
	obj();
	~obj();
	void setDiagBoundLength();

	float boundDiagLength;
	//stores count of each data type
	int vSize;
	int vtSize;
	int vnSize;
	int fSize;
	int eSize;

    float LODFactor;
	struct Pos
	{
		float x, y, z;
	};
	struct Vertex
	{
		float x, y, z;
		float importance;
		bool active = true;
		int collapsedTo;
		Pos cellLoc;
	};

	struct vTexture
	{
		float x, y;
	};

	struct vNormal
	{
		float x, y, z;
	};

	struct Face
	{
		int v1, v2, v3;
		int vt1, vt2, vt3;
		int vn1, vn2, vn3;
	};
	struct Edge
	{
		int v1, v2;
	};

	//lists for each type
	Vertex * vList;
	int * importanceList;
	vTexture * vtList;
	vNormal * vnList;
	Face * fList;
	Edge * eList;
	Pos position;

	Pos boundMinCorner;
	Pos boundMaxCorner;
	void SetEdgeList(int edgeCount, Edge * edgeList);
};

