#include <stdio.h>

#include "obj.h"
#include "objNewReader.h"
#include "GL\glut.h"
#include "MathLib.h"
#include "Vector.h"
#include <chrono>
#include "Matrix.h"
#include <random>


const int OBJLIMIT = 500;
int objCount = 0;
obj objectList[OBJLIMIT];
using namespace std::chrono;
float perSecondLastframe;
obj complexObjPREFAB(1,1,1,1);
obj ringPREFAB(1, 1, 1, 1);
obj cylinderPREFAB(1, 1, 1, 1);
obj slabPREFAB(1, 1, 1, 1);
float cellSize = 0.01;
objNewReader objReader;
MathLib myMaths;
float camX = 0;
float camY = 2;
float camZ = -10;
float camLookAtX = 0;
float camLookAtY = 0;
float camLookAtZ = 0;

float offSetX = 0;
float offSetY = 0;
float offSetZ = 0;

bool simple = false;
float fovY = 45;
float fovX = 0;
float maxDetailThreshold = 20;

int width = 1920;
int height = 1080;
float factor = 0.05;
Matrix deg1Rot;
int frameCount = 0;
Vector fwd(1,1,1);
Vector lookAt(1, 1, 1);
float length;
Matrix rot1;
int yAxisRotTot = 0;
int xAxisRotTot = 0;
void addObjToList(obj  inObject)
{
	if (objCount < OBJLIMIT)
	{
		objectList[objCount] = inObject;
		objCount++;
	}
}
void keyboardread(unsigned char key, int x, int y)
{
	switch (key)
	{
	default:
		break;
	case 'd':
		
		if (yAxisRotTot != 0 || yAxisRotTot > 0)
		{
			int next = yAxisRotTot - 1;
			yAxisRotTot = next % 360;
		}
		else
		{
			yAxisRotTot = 360;
		}

		camLookAtX = sin(float(yAxisRotTot)*(0.0175)) + camX;
		camLookAtY = sin(float(xAxisRotTot)*(0.0175)) + camY;
		camLookAtZ = cos(float(yAxisRotTot)*(0.0175)) *  cos(float(xAxisRotTot)*(0.0175)) + camZ;
		glutPostRedisplay();
		break;
	case 'D':
		if (yAxisRotTot != 0 || yAxisRotTot > 0)
		{
			int next = yAxisRotTot - 1;
			yAxisRotTot = next % 360;
		}
		else
		{
			yAxisRotTot = 360;
		}

		camLookAtX = sin(float(yAxisRotTot)*(0.0175)) + camX;
		camLookAtY = sin(float(xAxisRotTot)*(0.0175)) + camY;
		camLookAtZ = cos(float(yAxisRotTot)*(0.0175)) *  cos(float(xAxisRotTot)*(0.0175)) + camZ;
		glutPostRedisplay();
		break;
	case 'a':
		if (yAxisRotTot != 360 || yAxisRotTot < 360)
		{
			int next = yAxisRotTot + 1;
			yAxisRotTot = next % 360;
		}
		else
		{
			yAxisRotTot = 0;
		}

		camLookAtX = sin(float(yAxisRotTot)*(0.0175)) + camX;
		camLookAtY = sin(float(xAxisRotTot)*(0.0175)) + camY;
		camLookAtZ = cos(float(yAxisRotTot)*(0.0175)) *  cos(float(xAxisRotTot)*(0.0175)) + camZ;
		glutPostRedisplay();
		break;
	case 'A':
		if (yAxisRotTot != 360 || yAxisRotTot < 360)
		{
			int next = yAxisRotTot + 1;
			yAxisRotTot = next % 360;
		}
		else
		{
			yAxisRotTot = 0;
		}

		camLookAtX = sin(float(yAxisRotTot)*(0.0175)) + camX;
		camLookAtY = sin(float(xAxisRotTot)*(0.0175)) + camY;
		camLookAtZ = cos(float(yAxisRotTot)*(0.0175)) *  cos(float(xAxisRotTot)*(0.0175)) + camZ;
		glutPostRedisplay();
		break;
	case 'w':
		fwd = Vector(camLookAtX - camX, camLookAtY - camY, camLookAtZ - camZ);
		length = myMaths.vectorLength(fwd);
		fwd.vect[0] = fwd.vect[0] / length;
		fwd.vect[1] = fwd.vect[1] / length;
		fwd.vect[2] = fwd.vect[2] / length;
		camX = camX + fwd.vect[0];
		camY = camY + fwd.vect[1];
		camZ = camZ + fwd.vect[2];
		camLookAtX = camLookAtX + fwd.vect[0];
		camLookAtY = camLookAtY + fwd.vect[1];
		camLookAtZ = camLookAtZ + fwd.vect[2];
		glutPostRedisplay();
		break;
	case 'W':
		fwd = Vector(camLookAtX - camX, camLookAtY - camY, camLookAtZ - camZ);
		length = myMaths.vectorLength(fwd);
		fwd.vect[0] = fwd.vect[0] / length;
		fwd.vect[1] = fwd.vect[1] / length;
		fwd.vect[2] = fwd.vect[2] / length;
		camX = camX + fwd.vect[0];
		camY = camY + fwd.vect[1];
		camZ = camZ + fwd.vect[2];
		camLookAtX = camLookAtX + fwd.vect[0];
		camLookAtY = camLookAtY + fwd.vect[1];
		camLookAtZ = camLookAtZ + fwd.vect[2];
		glutPostRedisplay();
		break;
	case 's':
		fwd = Vector(camLookAtX - camX, camLookAtY - camY, camLookAtZ - camZ);
		length = myMaths.vectorLength(fwd);
		fwd.vect[0] = fwd.vect[0] / length;
		fwd.vect[1] = fwd.vect[1] / length;
		fwd.vect[2] = fwd.vect[2] / length;
		camX = camX - fwd.vect[0];
		camY = camY - fwd.vect[1];
		camZ = camZ - fwd.vect[2];
		camLookAtX = camLookAtX - fwd.vect[0];
		camLookAtY = camLookAtY - fwd.vect[1];
		camLookAtZ = camLookAtZ - fwd.vect[2];
		glutPostRedisplay();
		break;
	case 'S':
		fwd = Vector(camLookAtX - camX, camLookAtY - camY, camLookAtZ - camZ);
		length = myMaths.vectorLength(fwd);
		fwd.vect[0] = fwd.vect[0] / length;
		fwd.vect[1] = fwd.vect[1] / length;
		fwd.vect[2] = fwd.vect[2] / length;
		camX = camX - fwd.vect[0];
		camY = camY - fwd.vect[1];
		camZ = camZ - fwd.vect[2];
		camLookAtX = camLookAtX - fwd.vect[0];
		camLookAtY = camLookAtY - fwd.vect[1];
		camLookAtZ = camLookAtZ - fwd.vect[2];
		glutPostRedisplay();
		break;
	case 'u':
		camLookAtY = camLookAtY + 0.1;
		glutPostRedisplay();
		break;
	case 'U':
		camLookAtY = camLookAtY + 0.1;
		glutPostRedisplay();
		break;
	case 'j':
		camLookAtY = camLookAtY - 0.1;
		glutPostRedisplay();
		break;
	case 'J':
		camLookAtY = camLookAtY - 0.1;
		glutPostRedisplay();
		break;
	case 'i':
		camLookAtZ = camLookAtZ + 0.1;
		glutPostRedisplay();
		break;
	case 'I':
		camLookAtZ = camLookAtZ + 0.1;
		glutPostRedisplay();
		break;
	case 'k':
		camLookAtZ = camLookAtZ - 0.1;
		glutPostRedisplay();
		break;
	case 'K':
		camLookAtZ = camLookAtZ - 0.1;
		glutPostRedisplay();
		break;
	case 'o':
		camLookAtX = camLookAtX - 0.1;
		glutPostRedisplay();
		break;
	case 'O':
		camLookAtX = camLookAtX - 0.1;
		glutPostRedisplay();
		break;
	case 'p':
		camLookAtX = camLookAtX + 0.1;
		glutPostRedisplay();
		break;
	case 'P':
		camLookAtX = camLookAtX + 0.1;
		glutPostRedisplay();
		break;
	case '#':
		simple = false;
		glutPostRedisplay();
		break;
	case']':
		simple = true;
		glutPostRedisplay();
		break;
	}
}

void init(void)
{
	//OpenGL initialisation goes here
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glFrontFace(GL_CCW);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	myMaths.setRotYMat(&deg1Rot, 1);
}

void drawObj(obj object)
{
	glColor3f(1.0, 0.0, 0.0);
	bool red = false;
	for (int i = 0; i < object.fSize; i++)
	{
		if (red)
		{
			glColor3f(0.0, 0.0, 1.0);
			red = false;
		}
		else
		{
			glColor3f(1.0, 0.0, 0.0);
			red = true;
		}
		glBegin(GL_TRIANGLES);
		//finding point 1
		if (object.vList[object.fList[i].v1].active)
		{
			glVertex3f(object.vList[object.fList[i].v1].x + object.position.x, object.vList[object.fList[i].v1].y + object.position.y, object.vList[object.fList[i].v1].z + object.position.z);
		}
		else
		{
			glVertex3f(object.vList[object.vList[object.fList[i].v1].collapsedTo].x + object.position.x, object.vList[object.vList[object.fList[i].v1].collapsedTo].y + object.position.y, object.vList[object.vList[object.fList[i].v1].collapsedTo].z + object.position.z);
		}
		//finding point 2
		if (object.vList[object.fList[i].v2].active)
		{
			glVertex3f(object.vList[object.fList[i].v2].x + object.position.x, object.vList[object.fList[i].v2].y + object.position.y, object.vList[object.fList[i].v2].z + object.position.z);
		}
		else
		{
			glVertex3f(object.vList[object.vList[object.fList[i].v2].collapsedTo].x + object.position.x, object.vList[object.vList[object.fList[i].v2].collapsedTo].y + object.position.y, object.vList[object.vList[object.fList[i].v2].collapsedTo].z + object.position.z);
		}
		//finding point 3
		if (object.vList[object.fList[i].v3].active)
		{
			glVertex3f(object.vList[object.fList[i].v3].x + object.position.x, object.vList[object.fList[i].v3].y + object.position.y, object.vList[object.fList[i].v3].z + object.position.z);
		}
		else
		{
			glVertex3f(object.vList[object.vList[object.fList[i].v3].collapsedTo].x + object.position.x, object.vList[object.vList[object.fList[i].v3].collapsedTo].y + object.position.y, object.vList[object.vList[object.fList[i].v3].collapsedTo].z + object.position.z);
		}
		glEnd();
	}
}


void reshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fovY, width / height, 1.5f, 2000.0f);
	//fovy, aspect, near, far

	//calculating fovX from fovY
	//This is done by -
	//convert angle into pixels,
	//multiply pixels by aspect ratio
	//convert back into angle
	//Note that angles are converted into radians for the trig functions
	float halfvertFOV = fovY * 0.5f;
	float halfVertFOVRad = myMaths.conversionAngRad(halfvertFOV);
	float vertPixels = tan(halfVertFOVRad);
	float horizontalPix = (width / height) * vertPixels;
	float halfHorizontalFOVRad = atan(horizontalPix);
	float halfHorizontalFOV = myMaths.conversionAngDeg(halfHorizontalFOVRad);
	fovX = halfHorizontalFOV * 2.0f;
	glMatrixMode(GL_MODELVIEW);
}


void display()
{
	//count time at start and time at end to calc frame work time
	milliseconds timeStart = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	for (int a = 0; a < objCount; a++)
	{
		//Vector objCam(camX - objectList[a].position.x, camY - objectList[a].position.y, camZ - objectList[a].position.z);
		//float distFromCam = myMaths.vectorLength(objCam);
		//if (distFromCam > maxDetailThreshold)
		//{
		if (simple)
		{
			float size = 2.5 * objectList[a].LODFactor;
			objReader.collapseObject(&objectList[a], 100, size);
		}
		else
		{
			for (int o = 0; o < objectList[a].vSize; o++)
			{
				objectList[a].vList[o].active = true;
			}
		}
			
		//}
		//else
		//{
			//for (int o = 0; o < objectList[a].vSize; o++)
			//{
				//objectList[a].vList[o].active = true;
			//}
		//}
	}
	//clear background colour, so as last drawn buffer isn't shown
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0, 1.0, 1.0);
	glLoadIdentity();
	
	gluLookAt(camX, camY, camZ, camLookAtX, camLookAtY, camLookAtZ, 0.0, 1.0, 0.0);
	//first three - place camera, second three, aim camera at, last three, up vector
	glFrontFace(GL_CW);
	//we can add any functions that draw objects here
	for (int i = 0; i < objCount; i++)
	{
		drawObj(objectList[i]);
	}

	glutSwapBuffers();
	milliseconds timeEnd = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	milliseconds timeElapsed = timeEnd - timeStart;
	float floatMilli = timeElapsed.count();
	float perSecond = 1000 / floatMilli;
	float targert = 11.2;
	float upper = 8.4;
	if (floatMilli > targert)
	{
		for (int l = 0; l < objCount; l++)
		{
			Vector objCam(camX - objectList[l].position.x, camY - objectList[l].position.y, camZ - objectList[l].position.z);
			float distFromCam = myMaths.vectorLength(objCam);
			if (objectList[l].LODFactor < 2 && distFromCam > maxDetailThreshold)
			{
				objectList[l].LODFactor += 0.01;
				if (objectList[l].LODFactor > 2)
				{
					objectList[l].LODFactor = 2;
				}
			}
		}
	}
	else if (floatMilli < upper)
	{
		for (int l = 0; l < objCount; l++)
		{
			Vector objCam(camX - objectList[l].position.x, camY - objectList[l].position.y, camZ - objectList[l].position.z);
			float distFromCam = myMaths.vectorLength(objCam);
			if (objectList[l].LODFactor > 0 && distFromCam > maxDetailThreshold)
			{
				objectList[l].LODFactor -= 0.01;
				if (objectList[l].LODFactor < 0)
				{
					objectList[l].LODFactor = 0;
				}
			}
		}
	}
	system("CLS");
	printf("Franes: %f/n Milli: %f", perSecondLastframe, floatMilli);

}

void idleDisp()
{
	//count time at start and time at end to calc frame work time
	milliseconds timeStart = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	for (int a = 0; a < objCount; a++)
	{
		//Vector objCam(camX - objectList[a].position.x, camY - objectList[a].position.y, camZ - objectList[a].position.z);
		//float distFromCam = myMaths.vectorLength(objCam);
		//if (distFromCam > maxDetailThreshold)
		//{
		if (simple)
		{
			float size = 3 * objectList[a].LODFactor;
			objReader.collapseObject(&objectList[a], 100, size);
		}
		else
		{
			for (int o = 0; o < objectList[a].vSize; o++)
			{
				objectList[a].vList[o].active = true;
			}
		}
			
		//}
		//else
		//{
			//for (int o = 0; o < objectList[a].vSize; o++)
			//{
				//objectList[a].vList[o].active = true;
			//}
		//}
	}
	//clear background colour, so as last drawn buffer isn't shown
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0, 1.0, 1.0);
	glLoadIdentity();
	gluLookAt(camX, camY, camZ, camLookAtX, camLookAtY, camLookAtZ, 0.0, 1.0, 0.0);
	//first three - place camera, second three, aim camera at, last three, up vector
	glFrontFace(GL_CW);
	//we can add any functions that draw objects here
	for (int i = 0; i < objCount; i++)
	{
		drawObj(objectList[i]);
	}

	glutSwapBuffers();
	milliseconds timeEnd = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	milliseconds timeElapsed = timeEnd - timeStart;
	float floatMilli = timeElapsed.count();
	perSecondLastframe = 1000 / floatMilli;
	float targert = 11.2;
	float upper = 8.4;
	if (floatMilli > targert)
	{
		for (int l = 0; l < objCount; l++)
		{
			Vector objCam(camX - objectList[l].position.x, camY - objectList[l].position.y, camZ - objectList[l].position.z);
			float distFromCam = myMaths.vectorLength(objCam);
			if (objectList[l].LODFactor < 2 && distFromCam > maxDetailThreshold)
			{
				objectList[l].LODFactor += 0.01;
				if (objectList[l].LODFactor > 2)
				{
					objectList[l].LODFactor = 2;
				}
			}
		}
		
	}
	else if (floatMilli < upper)
	{
		
		for (int l = 0; l < objCount; l++)
		{
			Vector objCam(camX - objectList[l].position.x, camY - objectList[l].position.y, camZ - objectList[l].position.z);
			float distFromCam = myMaths.vectorLength(objCam);
			if (objectList[l].LODFactor > 0 && distFromCam > maxDetailThreshold)
			{
				objectList[l].LODFactor -= 0.1;
				if (objectList[l].LODFactor < 0)
				{
					objectList[l].LODFactor = 0;
				}
			}
		}
	}
	system("CLS");
	printf("Franes: %f/n Milli: %f", perSecondLastframe, floatMilli);
}


void main(int argc, char** argv)
{
	srand((unsigned)time(0));
	
	myMaths.setIDMat(&rot1);
	myMaths.setRotYMat(&rot1, 1);
	
	printf("Loading!\n");
	complexObjPREFAB = objReader.readObjFile("SmallerBoi.obj");
	ringPREFAB = objReader.readObjFile("ring.obj");
	slabPREFAB = objReader.readObjFile("ground.obj");
	cylinderPREFAB = objReader.readObjFile("entity.obj");
	printf("Done with obj reading!\n");

	printf("Printing object edge counts...\n %d", complexObjPREFAB.eSize);
	objReader.calculateVertexImportance(&complexObjPREFAB);
	objReader.calculateVertexImportance(&ringPREFAB);
	objReader.calculateVertexImportance(&slabPREFAB);
	objReader.calculateVertexImportance(&cylinderPREFAB);


	//setting up saaaaaaay 10? of these complexObjects for use
	//for (int b = 0; b < 2; b++)
	//{
	//	addObjToList(complexObjPREFAB);
	//}
	for (int f = 0; f < 500; f++)
	{
		addObjToList(objReader.readObjFile("ring.obj"));
		objReader.calculateVertexImportance(&objectList[f]);
	}
	for (int c = 0; c < objCount; c++)
	{
		float x = (rand() % 201) - 100;
		float y = rand() % 6;
		float z = (rand() % 201) - 100;
		objectList[c].position.x = x;
		objectList[c].position.y = y;
		objectList[c].position.z = z;
	}
	for (int i = 0; i < complexObjPREFAB.vSize; i++)
	{
		printf("Vertex %d Importance: %f\n", i, complexObjPREFAB.vList[i].importance);
	}

	//OpenGL Setup
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(1920, 1080);
	glutCreateWindow("Model Loader");
	init();
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboardread);
	glutIdleFunc(idleDisp);
	glutMainLoop();
}