#include "obj.h"
#include <math.h>
obj::obj(int inVSize, int inVTSize, int inVNSize, int inFSize)
{
	//set sizes
	vSize = inVSize;
	vtSize = inVTSize;
	vnSize = inVNSize;
	fSize = inFSize;
	LODFactor = 0.1;
	//set up lists
	vList = new Vertex[vSize];
	vtList = new vTexture[vtSize];
	vnList = new vNormal[vnSize];
	fList = new Face[fSize];
	importanceList = new int[vSize];

	position = { 0,0,0 };
	//filling vertex list with empties
	for (int i = 0; i < vSize; i++)
	{
		Vertex clean;
		clean.x = 0.0f;
		clean.y = 0.0f;
		clean.z = 0.0f;
		clean.importance = 0.0f;
		vList[i] = clean;
	}
}

obj::obj()
{
	//set sizes
	vSize = 1;
	vtSize = 1;
	vnSize = 1;
	fSize = 1;
	LODFactor = 0.1;
	//set up lists
	vList = new Vertex[vSize];
	vtList = new vTexture[vtSize];
	vnList = new vNormal[vnSize];
	fList = new Face[fSize];
	importanceList = new int[vSize];

	position = { 0,0,0 };
	//filling vertex list with empties
	for (int i = 0; i < vSize; i++)
	{
		Vertex clean;
		clean.x = 0.0f;
		clean.y = 0.0f;
		clean.z = 0.0f;
		clean.importance = 0.0f;
		vList[i] = clean;
	}
}

obj::~obj()
{

}

void obj::setDiagBoundLength()
{
	Vertex min;
	min.x = 0;
	min.y = 0;
	min.z = 0;
	Vertex max;
	max.x = 0;
	max.y = 0;
	max.z = 0;


	for (int i = 0; i < vSize; i++)
	{
		if (min.x > vList[i].x)
		{
			min.x = vList[i].x;
		}
		if (min.y > vList[i].y)
		{
			min.y = vList[i].y;
		}
		if (min.z > vList[i].z)
		{
			min.z = vList[i].z;
		}


		if (max.x < vList[i].x)
		{
			max.x = vList[i].x;
		}
		if (max.y < vList[i].y)
		{
			max.y = vList[i].y;
		}
		if (max.z < vList[i].z)
		{
			max.z = vList[i].z;
		}

	}

	boundDiagLength = sqrt(((max.x - min.x)*(max.x - min.x)) + ((max.y - min.y)*(max.y - min.y)) + ((max.z - min.z)*(max.z - min.z)));
	boundMinCorner = { min.x, min.y, min.z };
	boundMaxCorner = { max.x, max.y, max.z };
}

//creates storage for number of edges in object. Not done at initialisation - no way to determine exact number of edges based on vertices/faces
void obj::SetEdgeList(int edgeCount, Edge * edgeList)
{
	eList = new Edge[edgeCount];

	for (int i = 0; i < edgeCount; i++)
	{
		eList[i] = edgeList[i];
	}
	eSize = edgeCount;
	//bubblesort the list based on left vertex
	while (1)
	{
		bool swapped = false;
		Edge tempEdge;
		for (int j = 0; j < edgeCount - 1; j++)
		{
			if (eList[j + 1].v1 < eList[j].v1)
			{
				tempEdge = eList[j + 1];
				eList[j + 1] = eList[j];
				eList[j] = tempEdge;
				swapped = true;
			}
		}

		if (!swapped)
		{
			break;
		}
	}

}


