#include "objNewReader.h"

objNewReader::objNewReader()
{
	
}

objNewReader::~objNewReader()
{
}

obj objNewReader::readObjFile(char * inFile)
{
	//buffer for reading
	char buffer[50];
	FILE * objFile;
	int vCount = 0;
	int vtCount = 0;
	int vnCount = 0;
	int fCount = 0;
	//unsure if used
	int faceCornerCount;
	bool inFace = false;
	bool didRead = false;

	//open file
	objFile = fopen(inFile, "r");
	//scan in first line
	fscanf(objFile, "%s", buffer);

	//looping while not at EOF
	while (!feof(objFile))
	{
		//if buffer isn't whitespace, move on
		if (buffer != "")
		{
			//if buffer matches certain keys, take action
			if (strcmp(buffer, "v") == 0)
			{
				vCount++;
			}
			else if (strcmp(buffer, "vt") == 0)
			{
				vtCount++;
			}
			else if (strcmp(buffer, "vn") == 0)
			{
				vnCount++;
			}
			else if (strcmp(buffer, "f") == 0)
			{
				fCount++;
			}
			//here we would also check for mtls, but we're only fussed with mesh data right now
		}
		fscanf(objFile, "%s", buffer);
	}

	//internal obj to hole data and output when we're done
	obj object(vCount, vtCount, vnCount, fCount);
	//would also have read in mtlfile name from file in first pass and open it here

	//return to start of objfile
	rewind(objFile);

	//we're tracking data being put in to make sure it matches what was first counted here
	//also tracks which slot in the object array to dump data in
	int vIn = 0;
	int vtIn = 0;
	int vnIn = 0;
	int fIn = 0;

	obj::Edge * tempEdgeArray = new obj::Edge[fCount * 3];
	int eCount = 0;
	//here we would usually read in all the mtls and assign an ID to them. No need this time
	//read ahead in objfile
	fscanf(objFile, "%s", buffer);
	//while not at EOF again
	while (!feof(objFile))
	{
		//if string matches, copy data over
		//Wanted to use a case switch instead for this in previous version?? Check this
		if (strcmp(buffer, "v") == 0)
		{
			//if we see a v we know the next three are point data
			fscanf(objFile, "%f %f %f", &object.vList[vIn].x, &object.vList[vIn].y, &object.vList[vIn].z);
			//increase count
			vIn++;
		}
		else if (strcmp(buffer, "vt") == 0)
		{
			fscanf(objFile, "%f %f", &object.vtList[vtIn].x, &object.vtList[vtIn].y);
			vtIn++;
		}
		else if (strcmp(buffer, "vn") == 0)
		{
			fscanf(objFile, "%f %f %f", &object.vnList[vnIn].x, &object.vnList[vnIn].y, &object.vnList[vnIn].z);
			vnIn++;
		}
		//a check for usemtl exists in previous version - no need here
		else if (strcmp(buffer, "f") == 0)
		{
			//HEY! WOULD IT BE TOO MUCH TO DECOUPLE THIS?
			//previous version worked with quads - working with tris here
			//previous versions assigned mtlindex to faces - only concerned with mesh for now, not mtl
			
			//face data is formatted as f int/int/int int/int/int int/int/int
			//each set of three is v/vt/vn, refers to each vertex of a tri
			//following the f tag in file, we know three sets occur
			//so we read in each set, split that set, copy data

			//ADDITIONAL REQUIRED FOR VERTEX IMPORTANCE ALGORITHMS
			//We need to consider edge pairs, and the angle between them. Probably easiest to create edges in the same fashion as faces
			//Alternative is allow a vertex structure to point to all vertices that it shars an edge with. Going with a list first,
			//see no problems other than memory for larger objects. Edge creation occurs alongside vertex assignment in face structure below.

			obj::Edge tempEdge1;
			obj::Edge tempEdge2;
			obj::Edge tempEdge3;
			
			fscanf(objFile, "%s", buffer);
			//strtok needs to output data to somewhere, so we make that somewhere
			char * split = strtok(buffer, "/");
			//then copy this!
			//gotta convert the data to int tho, and drop by one to count from 0. yay for zeroes.
			object.fList[fIn].v1 = atoi(split) - 1;
			//setting same vertex to start of temp edges 1 and 3. Trying to keep edges organised with smaller vertex index on left - might make searching easier...
			tempEdge1.v1 = atoi(split) - 1;
			tempEdge3.v1 = atoi(split) - 1;

			//strtok remembers the set it's splitting - we use NULL to let it carry on through the data
			//overwriting split for each new strtok operation since we've gotten the data we need from it
			split = strtok(NULL, "/");
			object.fList[fIn].vt1 = atoi(split) - 1;
			split = strtok(NULL, "/");
			object.fList[fIn].vn1 = atoi(split) - 1;

			//and now we do it all again for sets 2 and 3
			fscanf(objFile, "%s", buffer);
			split = strtok(buffer, "/");
			object.fList[fIn].v2 = atoi(split) - 1;

			//vertex 1 connects to vertex 2, which connects to vertex 3 - we don't know vertex 3 right now, so just doing v1 - v2 edge (edge1) and starting edge 2
			if (tempEdge1.v1 > atoi(split) - 1)
			{
				//checking if incoming vertex is smaller and placing on left side of edge if it is...
				tempEdge1.v2 = tempEdge1.v1;
				tempEdge1.v1 = atoi(split) - 1;
			}
			else
			{
				//if not bigger, must be smaller - two face vertices cannot be the same vertex when output from maya - would just be an edge in itself
				tempEdge1.v2 = atoi(split) - 1;
			}

			//edge 2 beginning...
			tempEdge2.v1 = atoi(split) - 1;

			//continuing set...
			split = strtok(NULL, "/");
			object.fList[fIn].vt2 = atoi(split) - 1;
			split = strtok(NULL, "/");
			object.fList[fIn].vn2 = atoi(split) - 1;


			//set 3...
			fscanf(objFile, "%s", buffer);
			split = strtok(buffer, "/");
			object.fList[fIn].v3 = atoi(split) - 1;

			//edges 2 and 3 use the third vertex - checking both to see if this vertex index is smaller than the present ones
			if (tempEdge2.v1 > atoi(split) - 1)
			{
				tempEdge2.v2 = tempEdge2.v1;
				tempEdge2.v1 = atoi(split) - 1;
			}
			else
			{
				tempEdge2.v2 = atoi(split) - 1;
			}

			if (tempEdge3.v1 > atoi(split) - 1)
			{
				tempEdge3.v2 = tempEdge3.v1;
				tempEdge3.v1 = atoi(split) - 1;
			}
			else
			{
				tempEdge3.v2 = atoi(split) - 1;
			}

			//continuing set...
			split = strtok(NULL, "/");
			object.fList[fIn].vt3 = atoi(split) - 1;
			split = strtok(NULL, "/");
			object.fList[fIn].vn3 = atoi(split) - 1;

			//next - check if any of these edges already exist. We don't want to store a face's shared edge twice.
			//if eCount is 0 we know no edges have gone in and that we don't need to check - these are the first three existing edges.
			if (eCount != 0)
			{
				int staticCount = eCount;
				//for each edge, search the array and check if that edge already exists...
				//static count makes sure we're searching only the number of edges that exist before we put any of these new ones in.
				//no need to check any of the edges we may be putting in because we know they wont be the same as they're in this face together.
				bool existing1 = false;
				bool existing2 = false;
				bool existing3 = false;
				for (int i = 0; i < staticCount; i++)
				{
					if (tempEdgeArray[i].v1 == tempEdge1.v1 && tempEdgeArray[i].v2 == tempEdge1.v2)
					{
						existing1 = true;
					}

					if (tempEdgeArray[i].v1 == tempEdge2.v1 && tempEdgeArray[i].v2 == tempEdge2.v2)
					{
						existing2 = true;
					}

					if (tempEdgeArray[i].v1 == tempEdge3.v1 && tempEdgeArray[i].v2 == tempEdge3.v2)
					{
						existing3 = true;
					}
				}

				//once we've searched array - if the existing booleans are still false, the edge does not yet exist - we can plonk them in
				if (!existing1)
				{
					tempEdgeArray[eCount] = tempEdge1;
					eCount++;
				}

				if (!existing2)
				{
					tempEdgeArray[eCount] = tempEdge2;
					eCount++;
				}

				if (!existing3)
				{
					tempEdgeArray[eCount] = tempEdge3;
					eCount++;
				}
				//edge checks are done
			}
			else
			{
				//here is initial set of edges to be stored - we don't need to check if these exist because array is empty
				tempEdgeArray[eCount] = tempEdge1;
				eCount++;
				tempEdgeArray[eCount] = tempEdge2;
				eCount++;
				tempEdgeArray[eCount] = tempEdge3;
				eCount++;
			}

			//put that count up
			fIn++;
			//here previous version basically checked if an object was using quads and triangulated it into a new face
			//project isn't as fussed about that right now. Not bothering
		}
		fscanf(objFile, "%s", buffer);
	}
	//close that file
	fclose(objFile);

	//Before we return the object - we gotta add the edges in. This is done after all is said and done because we can't
	//determine the exact number of edges that exist from just vertex and face data, and setting space for the maximum possible number of edges
	//(i.e: faces * 3) is wasteful.

	object.SetEdgeList(eCount, tempEdgeArray);

	//get that object out into the world!
	//i.e. return object
	return object;
	//HEY. YOU'VE COUNTED HOW MANY DATA YOU COPIED. OUGHT TO ERROR CHECK IT BEFORE OUTPUT
}

//calculating vertex importance. Using a sum of longest edge and largest edge pair angle divided by 2 (Suggested by Low and Tan)
void objNewReader::calculateVertexImportance(obj * object)
{
	for (int i = 0; i < object->vSize; i++)
	{
		float longestEdgeDist = 0.0f;
		float largestSilhouetteProbability = 0.0f;
		int eCount = 0;
		obj::Edge * tempEdgearray = new obj::Edge[object->eSize];
		//calculating edge lengths first...
		for (int j = 0; j < object->eSize; j++)
		{
			//if left vertex is this vertex we're checking...
			if (object->eList[j].v1 == i || object->eList[j].v2 == i)
			{
				tempEdgearray[eCount] = object->eList[j];
				eCount++;

				float tempDist = 0.0f;
				tempDist = sqrt(((object->vList[object->eList[j].v2].x - object->vList[object->eList[j].v1].x) * (object->vList[object->eList[j].v2].x - object->vList[object->eList[j].v1].x))
					+ ((object->vList[object->eList[j].v2].y - object->vList[object->eList[j].v1].y) * (object->vList[object->eList[j].v2].y - object->vList[object->eList[j].v1].y))
					+ ((object->vList[object->eList[j].v2].z - object->vList[object->eList[j].v1].z) * (object->vList[object->eList[j].v2].z - object->vList[object->eList[j].v1].z)));

				if (tempDist > longestEdgeDist)
				{
					longestEdgeDist = tempDist;
				}
			}
			
		}
		delete[] tempEdgearray;
		//checking angles between edge pairs - for each edge in the temp list, check all edges after it - this checks all pairs...
		for (int k = 0; k < eCount; k++)
		{
			for (int l = k + 1; l < eCount; l++)
			{
				Vector edge1(object->vList[object->eList[k].v2].x - object->vList[object->eList[k].v1].x, object->vList[object->eList[k].v2].y - object->vList[object->eList[k].v1].y, object->vList[object->eList[k].v2].z - object->vList[object->eList[k].v1].z);
				Vector edge2(object->vList[object->eList[l].v2].x - object->vList[object->eList[l].v1].x, object->vList[object->eList[l].v2].y - object->vList[object->eList[l].v1].y, object->vList[object->eList[l].v2].z - object->vList[object->eList[l].v1].z);

				float Dot = myMaths.dotProd(edge1, edge2);
				float length1 = myMaths.vectorLength(edge1);
				float length2 = myMaths.vectorLength(edge2);
				float lengthMulti = length1 * length2;
				float cosAngle = Dot / lengthMulti;
				float radAngle = acos(cosAngle);

				float silProb = cos(radAngle / 2);
				if (silProb > largestSilhouetteProbability)
				{
					largestSilhouetteProbability = silProb;
				}
			}
		}

		object->vList[i].importance = largestSilhouetteProbability * longestEdgeDist;
	}

	for (int x = 0; x < object->vSize; x++)
	{
		object->importanceList[x] = x + 1;
	}

	//bubblesort importance list
	while (1)
	{
		bool swapped = false;

		for (int y = 0; y < object->vSize - 1; y++)
		{
			if (object->vList[object->importanceList[y + 1]].importance > object->vList[object->importanceList[y]].importance)
			{
				int temp = object->importanceList[y];
				object->importanceList[y] = object->importanceList[y + 1];
				object->importanceList[y + 1] = temp;
				swapped = true;
			}
		}

		if (!swapped)
		{
			break;
		}
	}
}


//efficient version of the below would set up a hash grid for vertices with cells being the requested cell size
//assign a hash key to each vertex using this grid then only adjacent cells need to be checked for vertices being collapsed...

//this function collapses objects using floating cell collapse (Low & Tan)
void objNewReader::collapseObject(obj * object, float percentage, float cellSize)
{
	int minVCount = object->vSize / 40;
	int inactive = 0;
	if (minVCount < 3)
	{
		minVCount = 3;
	}
	for (int a = 0; a < object->vSize; a++)
	{
		object->vList[a].active = true;
		float mod = fmod(object->vList[a].x, cellSize);
		float cellX = (object->vList[a].x - mod) / cellSize;
		mod = fmod(object->vList[a].y, cellSize);
		float cellY = (object->vList[a].y - mod) / cellSize;
		mod = fmod(object->vList[a].z, cellSize);
		float cellZ = (object->vList[a].z - mod) / cellSize;
		object->vList[a].cellLoc = { cellX, cellY, cellZ };
	}

	for (int i = 0; i < object->vSize; i++)
	{
		bool near = false;
		if (object->vList[object->importanceList[i]].active)
		{
			for (int j = i + 1; j < object->vSize; j++)
			{
				near = false;
				float diffX = object->vList[object->importanceList[i]].x - object->vList[object->importanceList[j]].x;
				if (diffX < 0)
				{
					diffX = -diffX;
				}
				if (diffX < cellSize)
				{
					float diffY = object->vList[object->importanceList[i]].y - object->vList[object->importanceList[j]].y;
					if (diffY < 0)
					{
						diffY = -diffY;
					}
					if (diffY < cellSize)
					{
						float diffZ = object->vList[object->importanceList[i]].z - object->vList[object->importanceList[j]].z;
						if (diffZ < 0)
						{
							diffZ = -diffZ;
						}
						if (diffZ < cellSize)
						{
							near = true;
						}
					}

					if (object->vList[object->importanceList[j]].active && near)
					{
						Vector edge(object->vList[object->importanceList[i]].x - object->vList[object->importanceList[j]].x, object->vList[object->importanceList[i]].y - object->vList[object->importanceList[j]].y, object->vList[object->importanceList[i]].z - object->vList[object->importanceList[j]].z);
						float dist = myMaths.vectorLength(edge);
						if (dist <= cellSize)
						{
							if (object->vSize - inactive > minVCount)
							{
								object->vList[object->importanceList[j]].active = false;
								object->vList[object->importanceList[j]].collapsedTo = object->importanceList[i];
								inactive++;
							}

						}
					}
				}
			}
		}
	}
}
