
#include "MathLib.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////																													   ////
////	MATHLIB WRITTEN FOR PREVIOUS ASSIGNMENT IN SECOND YEAR. REUSED AS IS WITH NO CHANGES FROM THERE. ALL WORK MINE.    ////
////																													   ////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
MathLib::MathLib()
{
	for (int i = 0; i < 1024; i++)
	{
		float scaledDeg = (360.0f / 1024.0f)*(static_cast<float>(i) + 1.0f);
		float scaledRad = MathLib::conversionAngRad(scaledDeg);
		lupSin[i] = sin(scaledRad);
		lupCos[i] = cos(scaledRad);
		//printf("COS %d: %f\n", i, lupCos[i]);
		//printf("SIN %d: %f\n",i, lupSin[i]);
	}
}

MathLib::~MathLib()
{
}

float MathLib::conversionAngRad(float degrees)
{
	float rad = degrees*(PI / 180);
	return rad;
}

float MathLib::conversionAngDeg(float Radians)
{
	float deg = Radians*(180 / PI);
	return deg;
}

float MathLib::getCos(float angleInDegrees)
{
	int slot = static_cast<int>((angleInDegrees * 1024) / 360);
	return lupCos[slot];
}

float MathLib::getSin(float angleInDegrees)
{
	int slot = static_cast<int>((angleInDegrees * 1024) / 360);
	return lupSin[slot];
}

Vector MathLib::multiplyMatVect(Matrix inMat, Vector inVect)
{

	Vector outVect(0,0,0);

	outVect.vect[0] = (inMat.mtrx[0][0] * inVect.vect[0]) + (inMat.mtrx[0][1] * inVect.vect[1]) + (inMat.mtrx[0][2] * inVect.vect[2]) + (inMat.mtrx[0][3] * inVect.vect[3]);
	outVect.vect[1] = (inMat.mtrx[1][0] * inVect.vect[0]) + (inMat.mtrx[1][1] * inVect.vect[1]) + (inMat.mtrx[1][2] * inVect.vect[2]) + (inMat.mtrx[1][3] * inVect.vect[3]);
	outVect.vect[2] = (inMat.mtrx[2][0] * inVect.vect[0]) + (inMat.mtrx[2][1] * inVect.vect[1]) + (inMat.mtrx[2][2] * inVect.vect[2]) + (inMat.mtrx[2][3] * inVect.vect[3]);
	outVect.vect[3] = (inMat.mtrx[3][0] * inVect.vect[0]) + (inMat.mtrx[3][1] * inVect.vect[1]) + (inMat.mtrx[3][2] * inVect.vect[2]) + (inMat.mtrx[3][3] * inVect.vect[3]);


	return outVect;
}

void MathLib::setIDMat(Matrix* inMat)
{
	for (int i = 0; i < 4; i++)
	{
		inMat -> mtrx[i][i] = 1;
	}
}

void MathLib::setTransMat(Matrix* inMat, float x, float y, float z)
{
	inMat -> mtrx[0][3] = x;
	inMat-> mtrx[1][3] = y;
	inMat-> mtrx[2][3] = z;
}

void MathLib::setRotXMat(Matrix* inMat, float angle)
{
	inMat-> mtrx[1][1] = getCos(angle);
	inMat-> mtrx[2][1] = getSin(angle);
	inMat-> mtrx[1][2] = -(getSin(angle));
	inMat-> mtrx[2][2] = getCos(angle);
}

void MathLib::setRotYMat(Matrix * inMat, float angle)
{
	inMat->mtrx[0][0] = getCos(angle);
	inMat->mtrx[2][0] = -(getSin(angle));
	inMat->mtrx[0][2] = getSin(angle);
	inMat->mtrx[2][2] = getCos(angle);
}

void MathLib::setRotZMat(Matrix* inMat, float angle)
{
	inMat->mtrx[0][0] = getCos(angle);
	inMat->mtrx[1][0] = getSin(angle);
	inMat->mtrx[0][1] = -(getSin(angle));
	inMat->mtrx[1][1] = getCos(angle);
}



void MathLib::setUniSclMat(Matrix* inMat, float scale)
{
	inMat->mtrx[3][3] = scale * 0.5;
}

void MathLib::setSclMat(Matrix* inMat, float scaleX, float scaleY, float scaleZ)
{
	inMat->mtrx[0][0] = scaleX;
	inMat->mtrx[1][1] = scaleY;
	inMat->mtrx[2][2] = scaleZ;
}

Matrix MathLib::multiplyMat(Matrix m1, Matrix m2)
{
	Matrix outMat;
	float currentTotal;
	currentTotal = (m1.mtrx[0][0] * m2.mtrx[0][0]) + (m1.mtrx[0][1] * m2.mtrx[1][0]) + (m1.mtrx[0][2] * m2.mtrx[2][0]) + (m1.mtrx[0][3] * m2.mtrx[3][0]);
	outMat.mtrx[0][0] = currentTotal;
	currentTotal = (m1.mtrx[0][0] * m2.mtrx[0][1]) + (m1.mtrx[0][1] * m2.mtrx[1][1]) + (m1.mtrx[0][2] * m2.mtrx[2][1]) + (m1.mtrx[0][3] * m2.mtrx[3][1]);
	outMat.mtrx[0][1] = currentTotal;
	currentTotal = (m1.mtrx[0][0] * m2.mtrx[0][2]) + (m1.mtrx[0][1] * m2.mtrx[1][2]) + (m1.mtrx[0][2] * m2.mtrx[2][2]) + (m1.mtrx[0][3] * m2.mtrx[3][2]);
	outMat.mtrx[0][2] = currentTotal;
	currentTotal = (m1.mtrx[0][0] * m2.mtrx[0][3]) + (m1.mtrx[0][1] * m2.mtrx[1][3]) + (m1.mtrx[0][2] * m2.mtrx[2][3]) + (m1.mtrx[0][3] * m2.mtrx[3][3]);
	outMat.mtrx[0][3] = currentTotal;

	currentTotal = (m1.mtrx[1][0] * m2.mtrx[0][0]) + (m1.mtrx[1][1] * m2.mtrx[1][0]) + (m1.mtrx[1][2] * m2.mtrx[2][0]) + (m1.mtrx[1][3] * m2.mtrx[3][0]);
	outMat.mtrx[1][0] = currentTotal;
	currentTotal = (m1.mtrx[1][0] * m2.mtrx[0][1]) + (m1.mtrx[1][1] * m2.mtrx[1][1]) + (m1.mtrx[1][2] * m2.mtrx[2][1]) + (m1.mtrx[1][3] * m2.mtrx[3][1]);
	outMat.mtrx[1][1] = currentTotal;
	currentTotal = (m1.mtrx[1][0] * m2.mtrx[0][2]) + (m1.mtrx[1][1] * m2.mtrx[1][2]) + (m1.mtrx[1][2] * m2.mtrx[2][2]) + (m1.mtrx[1][3] * m2.mtrx[3][2]);
	outMat.mtrx[1][2] = currentTotal;
	currentTotal = (m1.mtrx[1][0] * m2.mtrx[0][3]) + (m1.mtrx[1][1] * m2.mtrx[1][3]) + (m1.mtrx[1][2] * m2.mtrx[2][3]) + (m1.mtrx[1][3] * m2.mtrx[3][3]);
	outMat.mtrx[1][3] = currentTotal;

	currentTotal = (m1.mtrx[2][0] * m2.mtrx[0][0]) + (m1.mtrx[2][1] * m2.mtrx[1][0]) + (m1.mtrx[2][2] * m2.mtrx[2][0]) + (m1.mtrx[2][3] * m2.mtrx[3][0]);
	outMat.mtrx[2][0] = currentTotal;
	currentTotal = (m1.mtrx[2][0] * m2.mtrx[0][1]) + (m1.mtrx[2][1] * m2.mtrx[1][1]) + (m1.mtrx[2][2] * m2.mtrx[2][1]) + (m1.mtrx[2][3] * m2.mtrx[3][1]);
	outMat.mtrx[2][1] = currentTotal;
	currentTotal = (m1.mtrx[2][0] * m2.mtrx[0][2]) + (m1.mtrx[2][1] * m2.mtrx[1][2]) + (m1.mtrx[2][2] * m2.mtrx[2][2]) + (m1.mtrx[2][3] * m2.mtrx[3][2]);
	outMat.mtrx[2][2] = currentTotal;
	currentTotal = (m1.mtrx[2][0] * m2.mtrx[0][3]) + (m1.mtrx[2][1] * m2.mtrx[1][3]) + (m1.mtrx[2][2] * m2.mtrx[2][3]) + (m1.mtrx[2][3] * m2.mtrx[3][3]);
	outMat.mtrx[2][3] = currentTotal;

	currentTotal = (m1.mtrx[3][0] * m2.mtrx[0][0]) + (m1.mtrx[3][1] * m2.mtrx[1][0]) + (m1.mtrx[3][2] * m2.mtrx[2][0]) + (m1.mtrx[3][3] * m2.mtrx[3][0]);
	outMat.mtrx[3][0] = currentTotal;
	currentTotal = (m1.mtrx[3][0] * m2.mtrx[0][1]) + (m1.mtrx[3][1] * m2.mtrx[1][1]) + (m1.mtrx[3][2] * m2.mtrx[2][1]) + (m1.mtrx[3][3] * m2.mtrx[3][1]);
	outMat.mtrx[3][1] = currentTotal;
	currentTotal = (m1.mtrx[3][0] * m2.mtrx[0][2]) + (m1.mtrx[3][1] * m2.mtrx[1][2]) + (m1.mtrx[3][2] * m2.mtrx[2][2]) + (m1.mtrx[3][3] * m2.mtrx[3][2]);
	outMat.mtrx[3][2] = currentTotal;
	currentTotal = (m1.mtrx[3][0] * m2.mtrx[0][3]) + (m1.mtrx[3][1] * m2.mtrx[1][3]) + (m1.mtrx[3][2] * m2.mtrx[2][3]) + (m1.mtrx[3][3] * m2.mtrx[3][3]);
	outMat.mtrx[3][3] = currentTotal;
	return outMat;
}

Matrix MathLib::addMat(Matrix m1, Matrix m2)
{
	Matrix outMat;
	outMat.mtrx[0][0] = m1.mtrx[0][0] + m2.mtrx[0][0];
	outMat.mtrx[0][1] = m1.mtrx[0][1] + m2.mtrx[0][1];
	outMat.mtrx[0][2] = m1.mtrx[0][2] + m2.mtrx[0][2];
	outMat.mtrx[0][3] = m1.mtrx[0][3] + m2.mtrx[0][3];

	outMat.mtrx[1][0] = m1.mtrx[1][0] + m2.mtrx[1][0];
	outMat.mtrx[1][1] = m1.mtrx[1][1] + m2.mtrx[1][1];
	outMat.mtrx[1][2] = m1.mtrx[1][2] + m2.mtrx[1][2];
	outMat.mtrx[1][3] = m1.mtrx[1][3] + m2.mtrx[1][3];

	outMat.mtrx[2][0] = m1.mtrx[2][0] + m2.mtrx[2][0];
	outMat.mtrx[2][1] = m1.mtrx[2][1] + m2.mtrx[2][1];
	outMat.mtrx[2][2] = m1.mtrx[2][2] + m2.mtrx[2][2];
	outMat.mtrx[2][3] = m1.mtrx[2][3] + m2.mtrx[2][3];

	outMat.mtrx[3][0] = m1.mtrx[3][0] + m2.mtrx[3][0];
	outMat.mtrx[3][1] = m1.mtrx[3][1] + m2.mtrx[3][1];
	outMat.mtrx[3][2] = m1.mtrx[3][2] + m2.mtrx[3][2];
	outMat.mtrx[3][3] = m1.mtrx[3][3] + m2.mtrx[3][3];
	return outMat;
}

Matrix MathLib::transposeMat(Matrix inputMat)
{
	Matrix outMat;
	outMat.mtrx[0][0] = inputMat.mtrx[0][0];
	outMat.mtrx[0][1] = inputMat.mtrx[1][0];
	outMat.mtrx[0][2] = inputMat.mtrx[2][0];
	outMat.mtrx[0][3] = inputMat.mtrx[3][0];
	
	outMat.mtrx[1][0] = inputMat.mtrx[0][1];
	outMat.mtrx[1][1] = inputMat.mtrx[1][1];
	outMat.mtrx[1][2] = inputMat.mtrx[2][1];
	outMat.mtrx[1][3] = inputMat.mtrx[3][1];
	
	outMat.mtrx[2][0] = inputMat.mtrx[0][2];
	outMat.mtrx[2][1] = inputMat.mtrx[1][2];
	outMat.mtrx[2][2] = inputMat.mtrx[2][2];
	outMat.mtrx[2][3] = inputMat.mtrx[3][2];

	outMat.mtrx[3][0] = inputMat.mtrx[0][3];
	outMat.mtrx[3][1] = inputMat.mtrx[1][3];
	outMat.mtrx[3][2] = inputMat.mtrx[2][3];
	outMat.mtrx[3][3] = inputMat.mtrx[3][3];

	return outMat;
}

bool MathLib::checkOrth(Matrix inputMat)
{
	Matrix transMat = MathLib::transposeMat(inputMat);
	Matrix orthCheck = MathLib::multiplyMat(inputMat, transMat);
	Matrix idCheck;
	MathLib::setIDMat(&idCheck);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			if (orthCheck.mtrx[i][j] != idCheck.mtrx[i][j])
			{
				return false;
			}
		}
	}
	return true;
}

float MathLib::dotProd(Vector v1, Vector v2)
{
	float dotProd = (v1.vect[0] * v2.vect[0]) + (v1.vect[1] * v2.vect[1]) + (v1.vect[2] * v2.vect[2]) + (v1.vect[3] * v2.vect[3]);
	return dotProd;
}

float MathLib::vectorLength(Vector inVect)
{
	float length;
	length = (inVect.vect[0] * inVect.vect[0]) + (inVect.vect[1] * inVect.vect[1]) + (inVect.vect[2] * inVect.vect[2]) + (inVect.vect[3] * inVect.vect[3]);
	return sqrt(length);
}
