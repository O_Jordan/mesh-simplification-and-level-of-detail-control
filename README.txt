This project contains a .obj file loader, a mesh simplification algorithm using Low and Tan's version of vertex clustering and a level of detail control system.

This project was an investigation into developing a full level of detail system for mesh control. The results showed that this implementation of vertex clustering was not capable of running in a continuous framework and increasing frame rate. The mesh simplification algorithm itself ran successfully.
Further personal research should come as a result of this project.

CONTROLS
W - Move forwards
S - move backwards
A - turn left
D - turn right